.. Kursiv documentation master file, created by
   sphinx-quickstart on Thu Feb  6 21:57:40 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Välkommen till Kursivs dokumentation
=====================================

.. toctree::
   :maxdepth: 1

   overview
   create
   books
   settings
   bokhandeln

`Dokumentation <http://sphinx-doc.org/contents.html>`_
